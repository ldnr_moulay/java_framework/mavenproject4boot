// jQuery $(fonction) : n'executer que quand tout est pret
$(function() {
	// selectionner l'élément d'id "envoyer"
	$("#envoyer").on('click', function() {
		$("#message").html("Envoi !")
		let nom = $("#nom").val()
		let adultes = $("#adultes").val()
		let enfants = $("#enfants").val()
		let mois = $("#mois").val()
		let luxe = $("#luxe").prop("checked")
		//fonction ajax qui fait une requete ajax qui envoie toutes ces données vers .... en json
		$.ajax({
			url: "/envoi",
			type: "POST",
			dataType: "json",
			contentType: "application/json",
			data: JSON.stringify({
				"nom": nom,
				"adultes": adultes,
				"enfants": enfants,
				"mois": mois,
				"luxe": luxe
			})
			//Affichage de la reponse au niveau du client
		}).done(function(tarif) {
			$("#afficheResponse").html("Seulement " +
				tarif.prix + " €!")
		})
	})

})


