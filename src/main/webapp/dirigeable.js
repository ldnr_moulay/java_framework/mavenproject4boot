// jQuery $(fonction) : n'executer que quand tout est pret
$(function() {
	// selectionner l'élément d'id "envoyer"
	$("#envoyer").on('click', function() {
		$("#message").html("Envoi !")
		let nom = $("#nom").val()
		let tel = $("#tel").val()
		let email = $("#email").val()
		let places = $("#places").val()
		//fonction ajax qui fait une requete ajax qui envoie toutes ces données vers .... en json
		$.ajax({
			url: "/dirigeable/envoi",
			type: "POST",
			contentType: "application/json",
			//Le serveur renvoi une chaine brutdataType: "json",
			data: JSON.stringify({
				"nom": nom,
				"tel": tel,
				"email": email,
				"places": places,
			})
			//Affichage de la reponse au niveau du client
		}).done(function(retour) {
			$("#message").html("Recu  (" + retour + ")! ")//200, recupere ce que renvoi le controlleur à l'url /dirigeable/envoi'
		})
			.fail(function() {
				$("#message").html("Echec !")//400, 501...
			})

	})
	//clic sur "lire mes inscriptions"
	$("#lire").on('click',function(){
		$("#message").html("Lecture.....")
		let nom = $("#nom").val()
		$.ajax({
			url:"/dirigeable/inscriptions/"+nom,
			type:"GET",
			dataType:"json"
			})
			.done(function(retour) {
			$("#message").html("Recu  (" + retour.length + ") inscriptions ")//200, recupere ce que renvoi le controlleur à l'url /dirigeable/envoi'
			let lignes =""
			for(const ligne of retour){
				lignes += "<tr>"+
				"<td>"+ligne.tel+"</td>"+
				"<td>"+ligne.email+"</td>"+
				"<td>"+ligne.places+"</td>"+
				"</tr>"	
			}
			$("#inscriptions tbody").html(lignes)
		})
			.fail(function() {
				$("#message").html("Echec !")//400, 501...
			})
		
		
		
		
	})

})