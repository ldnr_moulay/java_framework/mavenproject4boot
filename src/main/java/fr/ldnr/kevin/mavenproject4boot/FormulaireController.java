package fr.ldnr.kevin.mavenproject4boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FormulaireController {
	// C'est un service web rest

	public static final Logger logger = LoggerFactory.getLogger(MontgolfieresMain.class);

	@RequestMapping("/health")
	public String health() {
		return "ok";
	}
	//FABRICATION DE LA REPONSE AU NIVEAU DU SERVEUR
	@ResponseBody //pour indiquer qu'ont renvoi une reponse sous forme d'un objet
	@RequestMapping("/envoi")
	public Tarif envoi(@RequestBody Formulaire formulaire) {// le formulaire se trouvera dans le corp de la requete
																// serveur, dans du json par exemple
		logger.info("Reçu :" + formulaire.getNom());
		Tarif t = new Tarif();
		t.setPrix(formulaire.getPrix());
		t.setMessage("ok");
		return t;

	}

}
