package fr.ldnr.kevin.mavenproject4boot;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dirigeable")
public class DirigeableController {
	public static final Logger logger = LoggerFactory.getLogger(MontgolfieresMain.class);
	
	//@Autowired // il va chercher dans tt les classes de configuration les sessionfactory, il va chercher par defaut les varible nommées sessionFactory de type SessionFactory
				//va chercher les beans
	//public SessionFactory sessionFactory;
	
	//deuxieme solution
	private SessionFactory sessionFactory;
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	

	//ECRITURE
	@RequestMapping(value="/envoi", method=RequestMethod.POST)
	public String envoi(@RequestBody Inscription inscription) { 
		logger.info("Inscription de  :" + inscription.getNom());
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(inscription);
		tx.commit();
		session.close();
		return "ok";
		
		
	}
	
	/**
	 * 
	 * APpel avec : localhost:8080/dirigeable/inscriptions/Bob
	 * LECTURE
	 */

	//on va utiliser le parametre pour qu'il soit une partie de l'url
	@RequestMapping("/inscriptions/{nom}")
	public List<Inscription> lire(@PathVariable String nom){
		logger.info("Get inscriptions de : "+nom);
		Session session = sessionFactory.openSession();
		String inscriptionHQL = "from Inscription where nom=:nom";
		List<Inscription> ins = session.createQuery(inscriptionHQL).setParameter("nom", nom).list();
		session.close();
		return ins;
		
		
	}

	
	
	

}
