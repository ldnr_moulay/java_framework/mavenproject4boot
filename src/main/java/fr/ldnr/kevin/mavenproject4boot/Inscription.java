package fr.ldnr.kevin.mavenproject4boot;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Table;
@Entity
 // @Table n'est pas obligatoire, il l'est seulement lorsqu'ont veut modifier quelque chose, par exemple le nom
public class Inscription implements Serializable{
	private int id;
	private String nom;
	private String tel;
	private String email;
	private int places;
	
	//les @Column ne sont pas obligatoires
	
	@Column(length=100, nullable=false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(length=100, nullable=false)
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	@Column(length=100, nullable=false)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(length=100, nullable=false)
	public int getPlaces() {
		return places;
	}
	public void setPlaces(int places) {
		this.places = places;
	}
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY) 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
