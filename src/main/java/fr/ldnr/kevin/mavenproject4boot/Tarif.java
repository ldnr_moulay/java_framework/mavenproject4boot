package fr.ldnr.kevin.mavenproject4boot;

public class Tarif {
	private double prix;
	private String message;

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
