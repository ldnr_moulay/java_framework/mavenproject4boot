package fr.ldnr.kevin.mavenproject4boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MontgolfieresMain {
	
	public static final Logger logger = LoggerFactory.getLogger(MontgolfieresMain.class);
	
	

	public static void main(String[] args) {
		
		SpringApplication.run(MontgolfieresMain.class, args);
		//apres le point c'est le niveau de log qu'ont veut 
		logger.debug("Debut");

	}

}
