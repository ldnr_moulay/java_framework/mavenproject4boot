package fr.ldnr.kevin.mavenproject4boot;

public class Formulaire {
	private String nom;
	private int adultes;
	private int enfants;
	private int mois;
	private boolean luxe;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAdultes() {
		return adultes;
	}

	public void setAdultes(int adultes) {
		this.adultes = adultes;
	}

	public int getEnfants() {
		return enfants;
	}

	public void setEnfants(int enfants) {
		this.enfants = enfants;
	}

	public int getMois() {
		return mois;
	}

	public void setMois(int mois) {
		this.mois = mois;
	}

	public boolean isLuxe() {
		return luxe;
	}

	public void setLuxe(boolean luxe) {
		this.luxe = luxe;
	}

	public double getPrix() {
		return adultes * 34 + enfants * 14;
	}

}
