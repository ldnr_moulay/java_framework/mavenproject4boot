package fr.ldnr.kevin.mavenproject4boot;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


@Configuration
public class HibernateConfiguration {
	
	public static final Logger logger = LoggerFactory.getLogger(MontgolfieresMain.class);
		@Bean
		public SessionFactory sessionFactory() {
			
			Properties options = new Properties();
			options.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
			options.put("hibernate.hbm2ddl.auto","create");
			options.put("hibernate.show_sql","true");
			options.put("hibernate.connection.driver_class","org.sqlite.JDBC");
			options.put("hibernate.connection.url","jdbc:sqlite:montgolfieres.sqlite");
			SessionFactory factory = new org.hibernate.cfg.Configuration().
					addProperties(options).addAnnotatedClass(Inscription.class).
					buildSessionFactory();
			
			logger.info("SessionFactory crée");
			return factory;
		}

	}
	
	
	
	


